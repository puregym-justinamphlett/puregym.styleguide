---
title: "Brand"
date: 2018-08-29T16:21:52+01:00
draft: false
menu: "main"
weight: "2"
---

<p class="para para--large">Brand guidelines from marketing and brand colours.</p>

<!--more-->

<a class="anchor" name="brand"></a>
<h2 class="heading heading--level3">Brand rules</h2>
<hr class="horizontal-rule" />
<p class="para para--large">Taken from the Brand evolution document 2015:</p>
<p class="para para--small para--italic">Although black and white are still the dominant colours in most cases, with the green being used to highlight, green is also now being used for one off, short term campaigns. The black is seen as very masculine, hence the incorporation of more white and the vibrant green. The logo must never be used on a green background.</p>
<p class="para para--small para--italic">Previously, green text has been used on a black background. We have learnt that the PureGym brand green is very hard to read on a black background. Only white text can be used on a black background, green and black text can be used on a white background. Green copy is still used to highlight important messages within a sentence.</p>
<a class="anchor" name="colours"></a>
<h2 class="heading heading--level3">Colours</h2>
<hr class="horizontal-rule" />
<div class="row text-center">
                    <div class="col-xs-12 col-sm-3">
                        <p class="para para--small padding-all-large background-brand-dark-color">$brand-dark-color</p>
                        <p class="para para--small padding-all-large background-brand-color">$brand-color</p>
                        <p class="para para--small padding-all-large background-brand-light-color">$brand-light-color</p>
                        <p class="para para--small padding-all-large background-brand-bright-color">$brand-bright-color</p>
                        <p class="para para--small padding-all-large background-brand-lightest-color">$brand-lightest-color</p>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <p class="para para--small padding-all-large background-brand-alt-color">$brand-alt-color</p>
                        <p class="para para--small padding-all-large background-brand-alt2-color">$brand-alt2-color</p>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <p class="para para--small padding-all-large background-shade-dark-color">$shade-dark-color</p>
                        <p class="para para--small padding-all-large background-shade-medium-color">$shade-medium-color</p>
                        <p class="para para--small padding-all-large background-shade-color">$shade-color</p>
                        <p class="para--small para padding-all-large background-shade-light-color">$shade-light-color</p>
                        <p class="para--small para padding-all-large background-shade-lightest-color">$shade-lightest-color</p>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <p class="para para--small padding-all-large background-error-color">$error-color</p>
                        <p class="para para--small padding-all-large background-success-color">$success-color</p>
                        <p class="para para--small padding-all-large background-secondary-color para--inverted">$secondary-color</p>
                        <p class="para para--small padding-all-large background-primary-color" style="border: 1px solid lightgrey;">$primary-color</p>
                    </div>
                </div>
