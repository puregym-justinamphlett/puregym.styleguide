# Hyde




Hyde is a brazen two-column [hugo](https://gohugo.io) theme based on the [Jekyll](http://jekyllrb.com) theme of the same name.
It pairs a prominent sidebar with uncomplicated content.

![Hyde screenshot](https://f.cloud.github.com/assets/98681/1831228/42af6c6a-7384-11e3-98fb-e0b923ee0468.png)


## Contents

- [Hyde](#hyde)
  - [Contents](#contents)
  - [Installation](#installation)
  - [Options](#options)
    - [Sidebar menu](#sidebar-menu)
    - [Sticky sidebar content](#sticky-sidebar-content)
  - [Google Analytics](#google-analytics)
  - [Author](#author)
  - [Ported By](#ported-by)
  - [License](#license)


## Installation

To install Hyde as your default theme, first install this repository in the `themes/` directory:

    $ cd themes/
    $ git clone https://github.com/spf13/hyde.git

Second, specify `hyde` as your default theme in the `config.toml` file. Just add the line

    theme = "hyde"

at the top of the file.


## Options

Hyde includes some customizable options, typically applied via classes on the `<body>` element.


### Sidebar menu

Create a list of nav links in the sidebar by assigning "menu=main" in the front matter.


### Sticky sidebar content

By default Hyde ships with a sidebar that affixes it's content to the bottom of the sidebar. You can optionally disabled this by removing the `.sidebar-sticky` class from the sidebar's `.container`. Sidebar content will then normally flow from top to bottom.

```html
<!-- Default sidebar -->
<div class="sidebar">
  <div class="container sidebar-sticky">
    ...
  </div>
</div>

<!-- Modified sidebar -->
<div class="sidebar">
  <div class="container">
    ...
  </div>
</div>
```



## Google Analytics

Google Analytics can be enabled by assigning your tracking code to the `googleAnalytics` variable in the config file:

**TOML**
```toml
googleAnalytics = "Your tracking code"
```

**YAML**
```yaml
googleAnalytics: Your tracking code
```

## Author
**Mark Otto**
- <https://github.com/mdo>
- <https://twitter.com/mdo>

## Ported By
**Steve Francia**
- <https://github.com/spf13>
- <https://twitter.com/spf13>

## License

Open sourced under the [MIT license](LICENSE.md).

<3
