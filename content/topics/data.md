---
title: "Data" 
date: 2018
draft: false
menu: "main"
weight: "2"
---

<p class="para para--large">Tables, class lists etc.</p>

<!--more-->

<a class="anchor" name="responsive-table"></a>
<h2 class="heading heading--level3 library-header">Responsive table</h2>
<hr class="horizontal-rule" />

<div class="example">
    <table class="details details--gymtime">
        <tbody class="details__body">
            <tr class="details__row">
                <td class="details__title details__title--50">Monday - Thursday</td>
                <td class="details__desc">10.00am - 11.30pm</td>
            </tr>
            <tr class="details__row">
                <td class="details__title details__title--50">Friday</td>
                <td class="details__desc">6.30am - 9.00pm</td>
            </tr>
            <tr class="details__row">
                <td class="details__title details__title--50">Saturday &amp; Sunday</td>
                <td class="details__desc">8.00am - 4.00pm</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/plain" class="language-html"><table class="details details--gymtime">
    <tbody class="details__body>
        <tr class="details__row">
            <td class="details__title details__title--50">Monday - Thursday</td>
            <td class="details__desc">10.00am - 11.30pm</td>
        </tr>
    </tbody>
</table>
</script>
 


<a class="anchor" name="class-lists"></a>
<h2 class="heading heading--level3 library-header">Class lists</h2>
<hr class="horizontal-rule" />

 <p class="para">Two methods of displaying our classes, both are similar on mobile, but whilst the first remains in a list view, the grid version will display 8 days horizontally, allowing members to see a whole week on one (desktop) screen.</p>
<div class="calendar calendar--list">
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Booked classes</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__date">Date</div>
                <div class="calendar-card__entry-time">Entry time</div>
                <div class="calendar-card__gym">Gym</div>
                <div class="calendar-card__class">Activity</div>
                <div class="calendar-card__duration">Duration</div>
            </div>
        </div>
        <div class="calendar-card calendar-card--static">
            <div class="calendar-card__date">Tue, 17th May</div>
            <div class="calendar-card__entry-time">17:00</div>
            <div class="calendar-card__gym">Aberdeen Kitty Brewster</div>
            <div class="calendar-card__class">Pure legs, bums &amp; tums</div>
            <div class="calendar-card__duration">45 mins</div>
        </div>
        <div class="calendar-card calendar-card--static">
            <div class="calendar-card__date">Tue, 17th May</div>
            <div class="calendar-card__entry-time">19:00</div>
            <div class="calendar-card__gym">Aberdeen Kitty Brewster</div>
            <div class="calendar-card__class">Pure Cycle</div>
            <div class="calendar-card__duration">45 mins</div>
        </div>
        <div class="calendar-card calendar-card--static">
            <div class="calendar-card__date">Thu, 19th May</div>
            <div class="calendar-card__entry-time">17:00</div>
            <div class="calendar-card__gym">Aberdeen Kitty Brewster</div>
            <div class="calendar-card__class">Weight Loss Induction</div>
            <div class="calendar-card__duration">30 mins</div>
        </div>
    </div>
</div>
<div class="calendar calendar--grid">
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Today</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <div class="calendar-card calendar-card--unavailable">
            <div class="calendar-card__class">Pure Tone</div>
            <div class="calendar-card__time">13:15</div>
            <div class="calendar-card__duration">30 mins</div>
            <div class="calendar-card__location">Studio</div>
            <div class="calendar-card__instructor">Sophie Every</div>
            <div class="calendar-card__status">Unavailable</div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Pure legs, bums &amp; tums</div>
            <div class="calendar-card__time">17:15</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Studio</div>
            <div class="calendar-card__instructor">Sunil Sunda</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--induction">Induction</span><span class="calendar-card__text calendar-card__text--limited">1 space left</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Tomorrow</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <div class="calendar-card">
            <div class="calendar-card__class">Pure fat burn</div>
            <div class="calendar-card__time">08:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Studio</div>
            <div class="calendar-card__instructor">Stanley Gonese</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--waiting-list">Waiting list</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Pure Cycle</div>
            <div class="calendar-card__time">16:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Cycle Studio</div>
            <div class="calendar-card__instructor">Gemma Green</div>
            <div class="calendar-card__status"><span class="calendar-card__text">9+ spaces</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Sat 15/08</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <a href="#" class="calendar-card calendar-card--waiting-list">
            <div class="calendar-card__class">Pure Cycle</div>
            <div class="calendar-card__time">07:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Cycle Studio</div>
            <div class="calendar-card__instructor">Jay Akarsi</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--on-waiting-list">Waiting list</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
        <a href="#" class="calendar-card calendar-card--booked">
            <div class="calendar-card__class">Tone Induction</div>
            <div class="calendar-card__time">08:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Studio</div>
            <div class="calendar-card__instructor">Gabriella McMahon</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--booked">Booked</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Sun 16/08</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">General Fitness Induction</div>
            <div class="calendar-card__time">16:00</div>
            <div class="calendar-card__duration">30 mins</div>
            <div class="calendar-card__location">Induction Point</div>
            <div class="calendar-card__instructor">Elaine Maitland</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--induction">Induction</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Pure Abs</div>
            <div class="calendar-card__time">17:30</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Gym Floor</div>
            <div class="calendar-card__instructor">Terry Dedman</div>
            <div class="calendar-card__status"><span class="calendar-card__text">9+ spaces</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Mon 17/08</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Pure Yoga</div>
            <div class="calendar-card__time">18:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Cycle Studio</div>
            <div class="calendar-card__instructor">Danny Barraclough</div>
            <div class="calendar-card__status"><span class="calendar-card__text">9+ spaces</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Weight Loss Induction</div>
            <div class="calendar-card__time">18:00</div>
            <div class="calendar-card__duration">30 mins</div>
            <div class="calendar-card__location">Induction Point</div>
            <div class="calendar-card__instructor">Elaine Maitland</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--induction">Induction</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Tue 18/08</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Weight Loss Induction</div>
            <div class="calendar-card__time">18:00</div>
            <div class="calendar-card__duration">30 mins</div>
            <div class="calendar-card__location">Induction Point</div>
            <div class="calendar-card__instructor">Elaine Maitland</div>
            <div class="calendar-card__status"><span class="calendar-card__text calendar-card__text--induction">Induction</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
        <div class="calendar-card calendar-card--unavailable">
            <div class="calendar-card__class">Pure Combat</div>
            <div class="calendar-card__time">08:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Studio</div>
            <div class="calendar-card__instructor">Stanley Gonese</div>
            <div class="calendar-card__status"><span class="calendar-card__text">Class full</span></div>
        </div>
    </div>
    <div class="calendar-col-md-1 calendar-column">
        <div class="calendar-column__title">
            <span>Wed 19/08</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <div class="calendar-card calendar-card--unavailable">
            <div class="calendar-card__class">Pure fat burn</div>
            <div class="calendar-card__time">08:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Studio</div>
            <div class="calendar-card__instructor">Stanley Gonese</div>
            <div class="calendar-card__status"><span class="calendar-card__text">Class full</span></div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Pure Cycle</div>
            <div class="calendar-card__time">16:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Cycle Studio</div>
            <div class="calendar-card__instructor">Gemma Green</div>
            <div class="calendar-card__status"><span class="calendar-card__text">9+ spaces</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
    <div class="calendar-column">
        <div class="calendar-column__title">
            <span>Thu 20/08</span>
        </div>
        <div class="calendar-column__head">
            <div class="calendar-card calendar-card--head">
                <div class="calendar-card__class">Class</div>
                <div class="calendar-card__time">Time</div>
                <div class="calendar-card__duration">Duration</div>
                <div class="calendar-card__location">Studio</div>
                <div class="calendar-card__instructor">Instructor</div>
                <div class="calendar-card__status">Class info</div>
            </div>
        </div>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Training for an Event Induction</div>
            <div class="calendar-card__time">17:30</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Gym Floor</div>
            <div class="calendar-card__instructor">Terry Dedman</div>
            <div class="calendar-card__status"><span class="calendar-card__text">9+ spaces</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
        <a href="#" class="calendar-card">
            <div class="calendar-card__class">Pure Cycle</div>
            <div class="calendar-card__time">18:00</div>
            <div class="calendar-card__duration">45 mins</div>
            <div class="calendar-card__location">Cycle Studio</div>
            <div class="calendar-card__instructor">Danny Barraclough</div>
            <div class="calendar-card__status"><span class="calendar-card__text">9+ spaces</span></div>
            <div class="calendar-card__overlay"><span class="calendar-card__overlay-text" aria-hidden="true" data-icon="&#187;"></span></div>
        </a>
    </div>
</div>