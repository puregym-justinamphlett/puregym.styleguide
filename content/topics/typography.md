---
title: "Typography" 
date: 2018
draft: false
menu: "main"
weight: "2"
---

<p class="para para--large">Typefaces, headings, paragraphs and lists.</p>

<!--more-->

<a class="anchor" name="fonts"></a>
<h2 class="heading heading--level3">Typefaces</h2>
<hr class="horizontal-rule" />
<p>We use two fonts. DIN for titles and CTAs, nearly always set to all-caps. Abel is the default site font and used for body copy. Neither typeface has supporting weights. Where Abel is set to bold or italic we rely upon the browser to interpret the base font. </p>
<div class="example">
<p class="para">
    <br/><span class="brand-regular-font">DIN CONDENSED BLACK</span>
    <br/><span>Abel</span>
</p>
</div>
<script type="text/plain" class="language-html"><span class="brand-regular-font">DIN CONDENSED BLACK</span>
<span>Abel</span>
</script>

<a class="anchor" name="headings"></a>
<h2 class="heading heading--level3">Headings</h2>
<hr class="horizontal-rule" />
<p>Style is disconnected from the tag you choose to use, any H tag can be styled to any size.</p>
<div class="example">
    <h1 class="heading heading--display">Display</h1>
    <h1 class="heading heading--display2">Display 2</h1>
    <h1 class="heading">Heading 1</h1>
    <h2 class="heading heading--level2">Heading 2</h2>
    <h3 class="heading heading--level3">Heading 3</h3>
    <h4 class="heading heading--level4">Heading 4</h4>
</div>
<script type="text/plain" class="language-html"><h1 class="heading heading–display"></h1>
<h1 class="heading heading–display2"></h1>
<h1 class="heading"></h1>
<h1 class="heading heading–level2"></h1>
<h1 class="heading heading–level3"></h1>
<h1 class="heading heading–level4"></h1>
</script>

<a class="anchor" name="paragraphs"></a>
<h2 class="heading heading--level3">Paragraphs</h2>
<hr class="horizontal-rule" />
<div class="example">
    <p class="para para--large">Paragraph large Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis.</p>
    <p class="para">Paragraph Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis.</p>
    <p class="para para--small">Paragraph small Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis.</p>
    <p class="para para--extra-small">Paragraph extra small Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis.</p>
</div>
<script type="text/plain" class="language-html"><p class="para para–large"></p>
<p class="para"></p>
<p class="para para–small"></p>
<p class="para para–extra-small"></p>
</script>


<a name="lists" class="anchor"></a>
<h2 class="heading heading--level3">Lists</h2>
<hr class="horizontal-rule" />
<div class="example">
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <ul class="list list--bullet list--small">
            <li class="list__item">Bullet item</li>
            <li class="list__item">Bullet item</li>
            <li class="list__item">Bullet item</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ol class="list list--numbered list--small">
            <li class="list__item">Numbered list item</li>
            <li class="list__item">Numbered list item</li>
            <li class="list__item">Numbered list item</li>
        </ol>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ul class="list list--check list--small">
            <li class="list__item">Checked item</li>
            <li class="list__item">Checked item</li>
            <li class="list__item">Checked item</li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <ul class="list list--bullet">
            <li class="list__item">Bullet item</li>
            <li class="list__item">Bullet item</li>
            <li class="list__item">Bullet item</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ol class="list list--numbered">
            <li class="list__item">Numbered list item</li>
            <li class="list__item">Numbered list item</li>
            <li class="list__item">Numbered list item</li>
        </ol>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ul class="list list--check">
            <li class="list__item">Checked item</li>
            <li class="list__item">Checked item</li>
            <li class="list__item">Checked item</li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <ul class="list list--bullet list--large">
            <li class="list__item">Bullet item</li>
            <li class="list__item">Bullet item</li>
            <li class="list__item">Bullet item</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ol class="list list--numbered list--large">
            <li class="list__item">Numbered list item</li>
            <li class="list__item">Numbered list item</li>
            <li class="list__item">Numbered list item</li>
        </ol>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ul class="list list--check list--large">
            <li class="list__item">Checked item</li>
            <li class="list__item">Checked item</li>
            <li class="list__item">Checked item</li>
        </ul>
    </div>
</div>
</div>
<script type="text/plain" class="language-html"><ul class="list list--bullet list--small">
    <li class="list__item">Bullet item in a small list</li>
</ul>
<ol class="list list--numbered list--large">
    <li class="list__item">Ordered item in a large list</li>
</ol>
<ul class="list list--check">
    <li class="list__item">Checked item</li>
</ul>
</script>