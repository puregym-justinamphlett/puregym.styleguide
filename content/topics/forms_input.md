---
title: "Forms & input"
date: 2018-08-29T16:21:52+01:00
draft: false
menu: "main"
weight: "2"
---

<p class="para para--large">Form elements such as input fields, custom checkboxes, and the search bar.</p>

<!--more-->

<a class="anchor" name="forms"></a>
<h2 class="heading heading--level3">Forms</h2>
<hr class="horizontal-rule" />
<div class="row">
    <div class="col-xs-12 col-md-6">
        <fieldset class="form__group">
            <label class="form__label" for="Email">Standard input &amp; label</label>
            <input class="form__input" type="email" name="email" id="Email" placeholder="Email Address" pattern="[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,64}$" maxLength={this.props.validation.maxLengthEmailAddress} title="Please complete your email address" required="required" />
        </fieldset>
        <fieldset class="form__group">
            <label class="form__label" for="Email">Error input</label>
            <input class="form__input form__input--error" type="email" name="email" id="Email" placeholder="Email Address" pattern="[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,64}$" maxLength={this.props.validation.maxLengthEmailAddress} title="Please complete your email address" required="required" />
            <p id="errorEMAILADDRESS" class="notification notification--error">We need your email address, this will be your login username</p>
        </fieldset>
        <fieldset class="form__group">
            <legend class="form__label form__label--legend">Radio select switch</legend>
            <radiogroup class="select-box">
                <input type="radio" value="male" name="gender" id="male" class="select-box__input">
                <label for="male" class="select-box__label select-box__label--inline">
                    Male
                </label>
                <input type="radio" value="female" name="gender" id="female" class="select-box__input">
                <label for="female" class="select-box__label select-box__label--inline">
                    Female
                </label>
            </radiogroup>
        </fieldset>
        <fieldset class="form__group">
            <input class="form__checkbox" id="disabledAccess" type="checkbox" name="isNewsletterAccepted" value="1" />
            <label class="form__label form__label--terms form__label--optionalfield" for="disabledAccess"><span class="form__input-span"></span><span class="form__checkbox-text">I require disabled access</span></label>
        </fieldset>
        <fieldset class="form__group">
            <label class="form__label" for="Email">Disabled input</label>
            <input class="form__input form__input--disabled" type="email" name="email" id="Email" placeholder="Email Address" pattern="[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,64}$" maxLength={this.props.validation.maxLengthEmailAddress} title="Please complete your email address" required="required" disabled/>
        </fieldset>
    </div>
</div>

<a class="anchor" name="search"></a>
<h2 class="heading heading--level3">Search block</h2>
<hr class="horizontal-rule" />
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="search">
            <form class="search__form">
                <div class="search__input-container">
                    <input class="search__input" type="text" name="search" placeholder="Enter your postcode or town" required />
                    <button class="search__reset" type="reset"></button>
                    <button class="search__button">
                        <span class="search__text">Search</span>
                    </button>
                </div>
                <hr class="search__horizontal-rule" />
                <button class="button button--wide button--small">
                    <svg class="icon icon--small icon--inverted" title="Geolocation" role="img"><use xlink:href="/svg/svg.svg#target" /></svg>
                     Find a gym near me
                </button>
            </form>
        </div>
    </div>
</div>