---
title: "Layout"
date: 2018-08-29T16:21:52+01:00
draft: false
menu: "main"
weight: "2"
---

<p class="para para--large">Structural blocks defining how page content is laid out.</p>

<!--more-->


<a class="anchor" name="page-content"></a>
<h2 class="heading heading--level3">Page content blocks</h2>
<hr class="horizontal-rule" />
<p class="para">Each page will usually use the following core structural elements inside the body tag. utility classes applied to __page&#95;&#95;content__ elements change the base background colour. This is how we generate the different blocks of full-width coloured content on a page.</p>
<script type="text/plain" class="language-html"><header class="page__header">
</header>
<main class="page__main">
    <div class="page__content background-shade-light-color">
    </div>
</main>
<footer class="page__footer">
</footer>
</script>


<a class="anchor" name="grid"></a>
<h2 class="heading heading--level3">Grid system</h2>
<hr class="horizontal-rule" />
<p class="para">The Bootstrap grid system is then used within these page blocks. This does not comform to BEM, having been taken wholesale from Bootstrap 3.3. The grid has no visual appearance, and is for the positioning of other content only. <a class="text-link" href="https://getbootstrap.com/docs/3.3/css/">For more info check out the original documentation</a>.</p>
<div class="notification notification--soft-error"><h3 class="heading heading--level4 negative-color">Common issues</h3>
If you have unusual padding issues, check that you don't have a row or container immediately nested within another (although it is ok to have a row within a column, which in turn has its own parent row). Or check that a row or container aren't missing altogether. The grid system is inflexible in the sense that you must always have the container/row/column structure, with the exception of the flex-block below.
</div>
<script type="text/plain" class="language-html"><main class="page__main">
    <div class="page__content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6"></div>
                <div class="col-xs-12 col-md-6"></div>
            </div>
        </div>
    </div>
</main>
</script>


<a class="anchor" name="flex"></a>
<h2 class="heading heading--level3">Flex block</h2>
<hr class="horizontal-rule" />
<p>CSS Flexbox gives us boxes that automatically resize their width or height to match their siblings. We have a couple of different specific blocks that take advantage of this and create their own grid layouts - the most versatile of these is flex-block. </p>

<p>Place this inside a row, replacing the need for Bootstrap columns. By default it shows 3 blocks per row on desktop, but this can be changed using a BEM modifier on the child items to two or four. There is a 'bottom' element for content to sit at the bottom of a block at all times regardless of its height, see the example below.</p>


<p class="para">Upgrading to the Bootstrap 4 grid may make this block obsolete.</p>
<div class="example">
    <ul class="flex-block">
        <li class="flex-block__item flex-block__item--four">
            <div class="flex-block__card background-shade-light-color">
                <div class="flex-block__content">
                    <h3 class="heading heading--level4">Student</h3>
                    <p class="para text-bold">Save up to 20% on student memberships with Unidays</p>
                    <p class="para para--small shade-dark-color">Staying fit during your studies has become a whole lot easier with our flexible student memberships. Save up to 20% when you sign-up with Unidays.</p>
                </div>
                <div class="flex-block__bottom">
                    <div class="flex-block__bottom-link">
                        Learn more
                    </div>
                </div>
            </div>
        </li>
        <li class="flex-block__item flex-block__item--four">
            <div class="flex-block__card background-shade-light-color">
                <div class="flex-block__content">
                    <h3 class="heading heading--level4">Day passes</h3>
                    <p class="para text-bold">Try out a gym for a day or week</p>
                    <p class="para para--small shade-dark-color">Not ready to commit to a full membership just yet? Choose a pass from 1 to 30 days for a single gym and discover what to expect at PureGym.</p>
                </div>
                <div class="flex-block__bottom">
                    <div class="flex-block__bottom-link">
                        Choose a gym
                    </div>
                </div>
            </div>
        </li>
        <li class="flex-block__item flex-block__item--four">
            <div class="flex-block__card background-shade-light-color">
                <div class="flex-block__content">
                    <h3 class="heading heading--level4">Fixed term</h3>
                    <p class="para text-bold">Save up to 10% on your membership when you pay in advance</p>
                    <p class="para para--small shade-dark-color">Make budgeting easier by paying in advance for access to a single gym for 6, 9 or 12 months. You can cancel you fixed-term membership anytime.</p>
                </div>
                <div class="flex-block__bottom">
                    <div class="flex-block__bottom-link">
                        Choose a gym
                    </div>
                </div>
            </div>
        </li>
        <li class="flex-block__item flex-block__item--four">
            <div class="flex-block__card background-shade-light-color">
                <div class="flex-block__content">
                    <h3 class="heading heading--level4">Corporate</h3>
                    <p class="para text-bold">Save up to 10% on memberships when you sign your employees up to one of our great corporate packages.</p>
                </div>
                <div class="flex-block__bottom">
                    <div class="flex-block__bottom-link">
                        Learn more
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<script type="text/plain" class="language-html"><div class="row">
    <ul class="flex-block">
        <li class="flex-block__item flex-block__item--four">
            <div class="flex-block__card">
                <div class="flex-block__content">
                    // Content here
                </div>
                <div class="flex-block__bottom">
                    // Footer link here
                </div>
            </div>
        </li>
    </ul>
</div>
</script>
        