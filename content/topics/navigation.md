---
title: "Navigation" 
date: 2018
draft: false
menu: "main"
weight: "2"
---

<p class="para para--large">Buttons, menus, tabs and links.</p>

<!--more-->

<a class="anchor" name="buttons"></a>
<h2 class="heading heading--level3">Buttons</h2>
<hr class="horizontal-rule" />
<div class="row">
    <div class="col-sm-6">
        <button class="button button--extra-small">Extra small button</button><br/>
        <button class="button button--small">Small button</button><br/>
        <button class="button">button</button><br/>
        <button class="button button--large">Large button</button><br/>
        <button class="button button--extra-large">Extra large button</button>
    </div>
    <div class="col-sm-6">
        <button class="button button--primary">Primary button</button><br/>
        <button class="button button--secondary">Secondary button</button><br/>
        <button class="button button--outline">Outline button</button><br/>
        <button class="button button--disabled">Disabled button</button>
    </div>
</div>
