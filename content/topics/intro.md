---
title: "Introduction"
date: 2018-08-29T16:21:52+01:00
draft: false
menu: "main"
weight: "1"
---

<p class="para para--large">Questions or comments? This pattern library is maintained by <a class="text-link" href="mailto:justin.amphlett@puregym.com">Justin Amphlett</a>.</p>

<!--more-->
<h2 class="heading heading--level3 padding-top">PureGym.com</h2>
<hr class="horizontal-rule" />
<p>The PureGym front-end HTML/CSS use the following:</p>
<p><img src="/img/sass.png" class="image" style="width:40px;margin-right:10px;" alt=""> <a href="https://sass-lang.com/">Sass: Syntactically Awesome Style Sheets</a></p>
<p><img src="/img/bem.png" class="image" style="width:40px;margin-right:10px;" alt=""> <a href="http://getbem.com/">BEM: Block Element Modifier</a></p>

<p>To avoid duplication, the processes by which we store and deploy the CSS, SVGs, fonts and images will be <a href="https://puregym.atlassian.net/wiki/spaces/PG/pages/572391560/Design+HTML+CSS+workflow">documented in PureGym Confluence</a>.</p>

<h2 class="heading heading--level3 padding-top">This pattern library</h2>
<hr class="horizontal-rule" />
<p>This site is stored on it's own Bitbucket repository.<br/><a href="https://bitbucket.org/puregym-justinamphlett/puregym.styleguide/src/master/" class="text-link">bitbucket.org/puregym-justinamphlett/puregym.styleguide/src/master/</a></p>
<p>It is built using <a href="https://gohugo.io/" class="text-link">Hugo</a>, a static site generator. The code examples are styled automatically using <a href="https://prismjs.com/" class="text-link">Prism</a>.</p>

<p>Pages are stored as Markdown documents, although currently due to familiarity the code is nearly all HTML. The left menu is automatically generated from these files.</p>

<p>To preview changes to the code locally, run a Hugo Server using the command line within the project folder and browse to <a href="localhost:1313" class="text-link">localhost:1313</a>. To publish, run the Hugo command once to generate the output folder, then FTP this to the required hosting. For more info visit <a href="https://gohugo.io/getting-started/usage/" class="text-link">gohugo.io/getting-started/usage/</a>.</p>

